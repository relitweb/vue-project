import Vue from 'vue'
import Vuex from 'vuex'
import users from '../users.json'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: users.users // ибо дали json =)
  },
  mutations: {

  },
  actions: {

  }
})
